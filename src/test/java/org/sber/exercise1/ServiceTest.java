package org.sber.exercise1;

import junit.framework.TestCase;
import org.sber.exercise2.ServiceWithTimeImpl;

import java.io.IOException;
import java.util.List;

public class ServiceTest extends TestCase {

    public void testDoWork() throws NoSuchMethodException, IOException {
        Service service1 =  CacheProxy.cache(new ServiceImpl());
        List<Integer> list1 = (List<Integer>) service1.doWork("work", 10, 5, 100); // возвращает результат сразу
        List<Integer> list2 = (List<Integer>) service1.doWork("work", 10, 5, 100); // возвращает результат из кэша

        assertEquals(list1, list2);

        Service service2 =  CacheProxy.cache(new ServiceImpl());
        ServiceWithTimeImpl object1 = (ServiceWithTimeImpl) service2.doWork("time"); // возвращает результат сразу

        ServiceWithTimeImpl object2 = (ServiceWithTimeImpl) service2.doWork("time"); // возвращает результат из кэша

        assertEquals(object1.getTimeCreate(), object2.getTimeCreate());

    }
}