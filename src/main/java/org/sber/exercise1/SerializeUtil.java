package org.sber.exercise1;

import java.io.*;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class SerializeUtil  {

    public static Object deserializeList(File file) throws IOException, ClassNotFoundException {
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            Object object =  objectInputStream.readObject();
            return object;
        }
    }

    public static void serializationList(Object object, String fileName) throws IOException {
        try ( ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileName))) {
            objectOutputStream.writeObject(object);
        }
    }

    public static void fileToZip(String fileName) throws IOException {
        try(ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(fileName + ".zip"));
            FileInputStream fileInputStream= new FileInputStream(fileName);) {
            ZipEntry entry = new ZipEntry(fileName);
            zipOutputStream.putNextEntry(entry);
            byte[] buffer = new byte[fileInputStream.available()];
            fileInputStream.read(buffer);
            zipOutputStream.write(buffer);
        }
    }

}
