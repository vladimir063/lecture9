package org.sber.exercise1;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


public class ServiceImpl implements Service {

    @Override
    public Object doWork(Object... args) throws NoSuchMethodException, IOException {
        int count = (int) args[1];
        int minValue = (int) args[2];
        int maxValue = (int) args[3];
        List<Integer> list = generateList(count, minValue, maxValue); // генерирую лист
        return list; // возвращаю лист
    }

    private List<Integer> generateList(int count, int minValue, int maxValue){
        int[] ints = new Random().ints(count, minValue, maxValue).toArray();
        return Arrays.stream(ints)
                .boxed()
                .collect(Collectors.toList());
    }
}
