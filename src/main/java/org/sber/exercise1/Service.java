package org.sber.exercise1;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface Service {

    @Cachable(zip = true)
    Object doWork(Object... args) throws NoSuchMethodException, IOException;
}
