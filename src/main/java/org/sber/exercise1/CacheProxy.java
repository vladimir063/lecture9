package org.sber.exercise1;


import java.io.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;


public class CacheProxy  implements InvocationHandler {

    private Service service;

    public CacheProxy(Service service) {
        this.service = service;
    }

    static Service cache(Service service) {
        return (Service) Proxy.newProxyInstance(Service.class.getClassLoader(), new Class[]{Service.class}, new CacheProxy(service));
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object[] arg = (Object[]) args[0];
        String fileName = (String) arg[0];
        File file = new File(fileName);
        if (file.exists()) {
            return SerializeUtil.deserializeList(file);
        }
        Object object =  method.invoke(service, args); // генерирую лист
        if (method.isAnnotationPresent(Cachable.class)){   // если есть анотация Cachable
            SerializeUtil.serializationList(object, fileName);  // сериализую лист в файл
            if (method.getAnnotation(Cachable.class).zip()){  // если есть включенная опция зип
                SerializeUtil.fileToZip(fileName);  // сохраняю файл в зип архив
            }
        }
        return object; // возвращаю лист
    }
}
