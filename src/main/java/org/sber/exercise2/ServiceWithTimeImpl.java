package org.sber.exercise2;

import org.sber.exercise1.Cachable;
import org.sber.exercise1.Service;

import java.io.*;
import java.time.LocalTime;


public class ServiceWithTimeImpl implements Service , Serializable {

    private LocalTime timeCreate = LocalTime.now();
    private LocalTime serializationTime;
    private transient LocalTime  deserializationTime;

    @Override
    public Object doWork(Object... args) throws NoSuchMethodException, IOException {
        ServiceWithTimeImpl serviceWithTime = new ServiceWithTimeImpl();
        return serviceWithTime;
    }

    private void writeObject(ObjectOutputStream out) throws IOException, InterruptedException {
        Thread.sleep(1000);
        serializationTime = LocalTime.now();
        out.defaultWriteObject();
    }
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException, InterruptedException {
        Thread.sleep(1000);
        deserializationTime = LocalTime.now();
        in.defaultReadObject();
    }

    public LocalTime getTimeCreate() {
        return timeCreate;
    }
}